package pokedex.konradgroup.com.pokedex.modules.baseComponents;

import android.content.Context;
import android.content.res.Resources;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class BaseActivity<BViewModel extends BaseViewModel, BBinding extends ViewDataBinding>
        extends AppCompatActivity implements BaseView {
    protected BBinding dataBinding;

    public abstract BViewModel getViewModel();

    public abstract void initializeDatabinding();

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // initializeDatabinding must be called before getViewModel() because to generate the viewModel needs the UI databinding
        initializeDatabinding();
        getViewModel().setView(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getViewModel().onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        getViewModel().onStop();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public FragmentActivity getCurrentActivity() {
        return this;
    }

    public View getRootView(){
        return dataBinding.getRoot();
    }

    @Override
    public Resources getSResources() {
        return getResources();
    }

}