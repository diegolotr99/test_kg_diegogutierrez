package pokedex.konradgroup.com.pokedex.repository.local;

import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Update;

import java.util.List;

public abstract class BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract void insert(T entity);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long insertAndGetId(T entity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAll(List<T> list);

    @Update
    public abstract void update(T object);

    @Delete
    public abstract void delete(T object);

}