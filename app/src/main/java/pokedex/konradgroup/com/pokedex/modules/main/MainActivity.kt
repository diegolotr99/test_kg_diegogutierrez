package pokedex.konradgroup.com.pokedex.modules.main

import android.os.Bundle
import pokedex.konradgroup.com.pokedex.modules.baseComponents.BaseActivity
import pokedex.konradgroup.com.pokedex.modules.pokemons.PokemonFragment
import pokedex.konradgroup.com.pokedex.R
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import pokedex.konradgroup.com.pokedex.modules.pokemons.DetailPokemonFragment

import pokedex.konradgroup.com.pokedex.databinding.ActivityMainBinding

class MainActivity: BaseActivity<MainViewModel, ActivityMainBinding>(), MainView, PokemonFragment.SelectedPokemonHandler  {
    override fun setNavigationTitle(title: String?) {
        supportActionBar?.title=title
    }

    private var frgPokemon: PokemonFragment? = null
    private val ROOT_FRAGMENT = "root_fragment"

    override fun getViewModel(): MainViewModel {
        return ViewModelProviders.of(this).get(MainViewModel::class.java)
    }

    override fun initializeDatabinding() {
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initFragmentInstances()
    }

    private fun initFragmentInstances() {
        frgPokemon = PokemonFragment()

        val fm = supportFragmentManager
        fm.beginTransaction()
                .add(R.id.fragmentContainer, frgPokemon)
                .addToBackStack(ROOT_FRAGMENT)
                .show(frgPokemon)
                .commit()
    }

    override fun showPokemon(pokemonId: Long, pokemonServerId: String) {
        val fragment = DetailPokemonFragment.newInstance(pokemonId, pokemonServerId)
        val fm = supportFragmentManager
        fm.beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .addToBackStack(DetailPokemonFragment::class.java.getSimpleName())
                .show(fragment)
                .commit()
    }

}
