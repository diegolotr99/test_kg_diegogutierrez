package pokedex.konradgroup.com.pokedex.utils;

public class PokemonSpritesHelper {
    public static final String BACK_DEFAULT_URL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/";
    public static final String BACK_SHINY_URL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/";
    public static final String FRONT_DEFAULT_URL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/";
    public static final String FRONT_SHINY_URL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/";
}
