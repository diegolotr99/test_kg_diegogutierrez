package pokedex.konradgroup.com.pokedex.repository.local;


import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import pokedex.konradgroup.com.pokedex.BuildConfig;
import pokedex.konradgroup.com.pokedex.models.Pokemon;

@Database(entities = {
        Pokemon.class},
        version = 1)
public abstract class AppDB extends RoomDatabase {

    private static AppDB instance;

    public static void getAppDatabase(Context context) {
        if (instance == null) {
            instance =
                    Room.databaseBuilder(context.getApplicationContext(),
                            AppDB.class, BuildConfig.POKEWORLD_DATABASE_NAME)
                            .allowMainThreadQueries()
                            .build();
        }
    }

    public static AppDB getInstance() {
            return instance;
    }
    public abstract PokemonsDao pokemonsDao();


}
