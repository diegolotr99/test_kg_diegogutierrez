package pokedex.konradgroup.com.pokedex.models.ResponseModels;

import java.util.List;

import pokedex.konradgroup.com.pokedex.models.Pokemon;

public class PokemonListResponse {
    List<Pokemon> results;

    public List<Pokemon> getResults() {
        return results;
    }

    public void setResults(List<Pokemon> results) {
        this.results = results;
    }
}
