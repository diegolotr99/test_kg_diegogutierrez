package pokedex.konradgroup.com.pokedex.modules.pokemons;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pokedex.konradgroup.com.pokedex.modules.baseComponents.BaseFragment;
import pokedex.konradgroup.com.pokedex.R;
import pokedex.konradgroup.com.pokedex.databinding.FragmentPokemonsBinding;

public class PokemonFragment extends BaseFragment<PokemonViewModel, FragmentPokemonsBinding> implements PokemonsView {
    SelectedPokemonHandler callback;
    private SwipeRefreshLayout swipeRefreshLayout;

    public interface SelectedPokemonHandler {
        void showPokemon(long pokemonId, String pokemonServerId);
    }
    @Override
    public PokemonViewModel getViewModel() {

        return ViewModelProviders.of(this).get(PokemonViewModel.class);
    }
    public PokemonFragment() {
        // Super is required to execute default action on each Fragment
        super();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SelectedPokemonHandler) {
            callback = (SelectedPokemonHandler) context;
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        dataBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_pokemons, container, false);

        getViewModel().loadPokemonList(dataBinding.rvPokemons);

        initializeSwipeRefreshLayout();

        return dataBinding.getRoot();
    }
        @Override
    public void showPokemon(long pokemonId, String pokemonServerId) {
            callback.showPokemon(pokemonId, pokemonServerId);

    }
        private void initializeSwipeRefreshLayout() {
            swipeRefreshLayout = dataBinding.getRoot().findViewById(R.id.swipe_refresh);
            swipeRefreshLayout.setColorSchemeColors(Color.YELLOW, Color.RED, Color.GREEN, Color.BLUE);
            swipeRefreshLayout.setOnRefreshListener(() -> getViewModel().loadPokemonList(dataBinding.rvPokemons));
        }
    @Override
    public void hideRefreshLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }
}
