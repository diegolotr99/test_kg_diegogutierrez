package pokedex.konradgroup.com.pokedex.modules.baseComponents;

import android.support.v7.widget.RecyclerView;

public abstract class BaseAdapter<VIEW extends BaseView> extends RecyclerView.Adapter  {
    protected VIEW view;

    public BaseAdapter(VIEW view) {
        this.view = view;
    }

    public BaseAdapter() {
    }
}