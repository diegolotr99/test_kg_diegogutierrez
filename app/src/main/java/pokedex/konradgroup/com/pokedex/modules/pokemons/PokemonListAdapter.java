package pokedex.konradgroup.com.pokedex.modules.pokemons;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.databinding.DataBindingUtil;

import java.util.Collections;
import java.util.List;

import pokedex.konradgroup.com.pokedex.models.Pokemon;
import pokedex.konradgroup.com.pokedex.R;
import pokedex.konradgroup.com.pokedex.modules.baseComponents.BaseViewHolder;
import pokedex.konradgroup.com.pokedex.modules.baseComponents.BaseAdapter;
import pokedex.konradgroup.com.pokedex.databinding.ItemPokemonBinding;

public class PokemonListAdapter extends BaseAdapter<PokemonsView> {


    private List<Pokemon> itemList;

    public PokemonListAdapter(PokemonsView view) {
            super(view);
            this.itemList = Collections.emptyList();
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            ItemPokemonBinding itemPokemonBinding =
                    DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_pokemon,
                            parent, false);
            return new PokemonAdapterViewHolder(itemPokemonBinding, view);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            ((PokemonAdapterViewHolder) holder).bindRouteModel(itemList.get(position));
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public int getItemCount() {
            return itemList.size();
        }

        public void setPokemonList(List<Pokemon> list) {
            this.itemList = list;
            notifyDataSetChanged();
        }

        static class PokemonAdapterViewHolder extends BaseViewHolder {
            ItemPokemonBinding itemPokemonBinding;
            PokemonsView pokemonsView;

            PokemonAdapterViewHolder(final ItemPokemonBinding itemPokemonBinding, PokemonsView pokemonsView) {
                super(itemPokemonBinding.itemPokemon);
                this.itemPokemonBinding = itemPokemonBinding;
                this.pokemonsView = pokemonsView;
            }

            void bindRouteModel(Pokemon pokemon) {
                if (itemPokemonBinding.getPokemonItemPresenter() == null) {
                    itemPokemonBinding.setPokemonItemPresenter(
                            new PokemonItemPresenter(pokemon, pokemonsView));
                } else {
                    itemPokemonBinding.getPokemonItemPresenter().setPokemon(pokemon);
                }
            }
        }
}
