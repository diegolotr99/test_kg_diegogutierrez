package pokedex.konradgroup.com.pokedex.modules.main;

import pokedex.konradgroup.com.pokedex.modules.baseComponents.BaseView;

public interface MainView extends BaseView {
    void setNavigationTitle(final String title);
}