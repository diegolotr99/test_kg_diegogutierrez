package pokedex.konradgroup.com.pokedex.models;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = "pokemons")
public class Pokemon {
    @PrimaryKey(autoGenerate = true)
    private long localId;
    private String name;
    private String url;
    private long weight;
    @SerializedName("base_experience")
    private long baseExperience;


    public long getLocalId() {
        return localId;
    }

    public void setLocalId(long localId) {
        this.localId = localId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    public String getId(){
        String purl =url.substring(0, url.length() - 1);
        String id= purl.substring(purl.lastIndexOf("/") + 1);
        return id;
    }

    public long getWeight() {
        return weight;
    }

    public void setWeight(long weight) {
        this.weight = weight;
    }

    public long getBaseExperience() {
        return baseExperience;
    }

    public void setBaseExperience(long baseExperience) {
        this.baseExperience = baseExperience;
    }
}
