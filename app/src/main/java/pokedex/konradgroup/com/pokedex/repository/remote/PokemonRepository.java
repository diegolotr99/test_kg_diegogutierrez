package pokedex.konradgroup.com.pokedex.repository.remote;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.List;

import pokedex.konradgroup.com.pokedex.models.Pokemon;
import pokedex.konradgroup.com.pokedex.models.ResponseModels.PokemonListResponse;
import pokedex.konradgroup.com.pokedex.modules.baseComponents.BaseRepository;
import pokedex.konradgroup.com.pokedex.repository.local.AppDB;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_OK;

public class PokemonRepository extends BaseRepository{
    private static final String TAG = "PokemonsRepository";
    PokemonsInterface getRestInterface() {
        return retrofit.create(PokemonsInterface.class);
    }
    public void getPokemons() {
        PokemonsInterface pokemonsInterface = getRestInterface();

        pokemonsInterface.getAll().enqueue(new Callback<PokemonListResponse>() {
            @Override
            public void onResponse(Call<PokemonListResponse> call, Response<PokemonListResponse> response) {
                if (response.code() == HTTP_OK) {
                    PokemonListResponse pokemons = response.body();
                    AppDB.getInstance().pokemonsDao().insertAll(pokemons.getResults());

                }
            }

            @Override
            public void onFailure(Call<PokemonListResponse> call, Throwable t) {
                Log.e(TAG, "Error al recibir los Pokemones");
            }

        });
    }

    public void getPokemonThumbnailById(String pokemonId) {
        PokemonsInterface pokemonsInterface = getRestInterface();

        pokemonsInterface.getThumbnailById(pokemonId).enqueue(new Callback<Pokemon>() {
            @Override
            public void onResponse(Call<Pokemon> call, Response<Pokemon> response) {
                if (response.code() == HTTP_OK) {
                    Pokemon pokemon = response.body();
                    pokemon.setLocalId(AppDB.getInstance().pokemonsDao().getIdByName(pokemon.getName()));
                    AppDB.getInstance().pokemonsDao().update(pokemon);

                }
            }

            @Override
            public void onFailure(Call<Pokemon> call, Throwable t) {
                Log.e(TAG, "Error al recibir los Pokemones");
            }

        });
    }


    public void getPokemonDetails(String pokemonId) {
        PokemonsInterface pokemonsInterface = getRestInterface();

        pokemonsInterface.getDetailsById(pokemonId).enqueue(new Callback<Pokemon>() {
            @Override
            public void onResponse(Call<Pokemon> call, Response<Pokemon> response) {
                if (response.code() == HTTP_OK) {

                    Pokemon pokemonResponse = response.body();
                    Pokemon pokemonExistent = AppDB.getInstance().pokemonsDao().getByName(pokemonResponse.getName());
                    if(pokemonExistent!=null){
                        pokemonExistent.setWeight(pokemonResponse.getWeight());
                        pokemonExistent.setBaseExperience(pokemonResponse.getBaseExperience());
                    }
                    AppDB.getInstance().pokemonsDao().update(pokemonExistent);

                }
            }

            @Override
            public void onFailure(Call<Pokemon> call, Throwable t) {
                Log.e(TAG, "Error al recibir los Pokemones");
            }

        });
    }
}
