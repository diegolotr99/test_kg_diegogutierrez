package pokedex.konradgroup.com.pokedex.modules.main;

public class SetTitleEvent {
    private final String title;

    public SetTitleEvent(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}