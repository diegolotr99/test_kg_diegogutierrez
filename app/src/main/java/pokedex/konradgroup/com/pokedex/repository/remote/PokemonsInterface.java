package pokedex.konradgroup.com.pokedex.repository.remote;

import pokedex.konradgroup.com.pokedex.models.Pokemon;
import pokedex.konradgroup.com.pokedex.models.ResponseModels.PokemonListResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PokemonsInterface {
    @GET("pokemon/")
    Call<PokemonListResponse> getAll();

    @GET("pokemon-form/{pokemonId}")
    Call<Pokemon> getThumbnailById(@Path("pokemonId") String pokemonId);

    @GET("pokemon/{pokemonId}")
    Call<Pokemon> getDetailsById(@Path("pokemonId") String pokemonId);
}
