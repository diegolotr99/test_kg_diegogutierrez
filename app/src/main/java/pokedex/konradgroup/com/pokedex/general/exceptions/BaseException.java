package pokedex.konradgroup.com.pokedex.general.exceptions;

public abstract class BaseException extends Exception {
    public BaseException(String message) {
        super(message);
    }
}
