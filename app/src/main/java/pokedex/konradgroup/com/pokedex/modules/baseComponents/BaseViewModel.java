package pokedex.konradgroup.com.pokedex.modules.baseComponents;
import android.arch.lifecycle.ViewModel;

import pokedex.konradgroup.com.pokedex.general.PokeWorld;
import pokedex.konradgroup.com.pokedex.modules.main.SetTitleEvent;

public abstract class BaseViewModel<BView extends BaseView> extends ViewModel {
    private BView bView;

    public void setView(BView bView) {
        this.bView = bView;
    }

    public BView getView() {
        return bView;
    }

    public void setTitle(final String title){
        PokeWorld.getEventBus().post(new SetTitleEvent(title));
    }

    public abstract void onStart();
    public abstract void onStop();
}
