package pokedex.konradgroup.com.pokedex.repository.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.lifecycle.LiveData;

import java.util.List;

import pokedex.konradgroup.com.pokedex.general.PokeWorld;
import pokedex.konradgroup.com.pokedex.models.Pokemon;

@Dao
public abstract class PokemonsDao extends BaseDao<Pokemon> {

    @Query("SELECT * FROM pokemons")
    public abstract LiveData<List<Pokemon>> getAll();

    @Query("SELECT * FROM pokemons")
    public abstract List<Pokemon> getAllPokemons();

    @Query("Select localId from pokemons where name=:pokemonName")
    public abstract long getIdByName(String pokemonName);

    @Query("Select * from pokemons where name=:pokemonName")
    public abstract Pokemon getByName(String pokemonName);


    @Query("SELECT * FROM pokemons WHERE localId=:pokemonId")
    public abstract Pokemon getById(final long pokemonId);
}
