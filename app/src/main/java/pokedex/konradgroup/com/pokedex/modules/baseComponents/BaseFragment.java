package pokedex.konradgroup.com.pokedex.modules.baseComponents;

import android.content.Context;
import android.content.res.Resources;
import android.databinding.ViewDataBinding;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;

public abstract class BaseFragment<BViewModel extends BaseViewModel, BBinding extends ViewDataBinding> extends Fragment implements BaseView {
    protected BBinding dataBinding;

    public abstract BViewModel getViewModel();

    @SuppressWarnings("unchecked")
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        getViewModel().setView(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        getViewModel().onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        getViewModel().onStop();
    }

    @Override
    public FragmentActivity getCurrentActivity() {
        return getActivity();
    }

    @Override
    public View getRootView() {
        return dataBinding.getRoot();
    }

    @Override
    public Resources getSResources() {
        return getResources();
    }
}