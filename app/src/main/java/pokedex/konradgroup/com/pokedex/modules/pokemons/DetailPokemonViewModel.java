package pokedex.konradgroup.com.pokedex.modules.pokemons;

import pokedex.konradgroup.com.pokedex.models.Pokemon;
import pokedex.konradgroup.com.pokedex.modules.baseComponents.BaseViewModel;
import pokedex.konradgroup.com.pokedex.general.PokeWorld;
import pokedex.konradgroup.com.pokedex.modules.pokemons.DetailPokemonView;
import pokedex.konradgroup.com.pokedex.repository.local.AppDB;

public class DetailPokemonViewModel extends BaseViewModel<DetailPokemonView> {

    public Pokemon getPokemon(long pokemonId){
       return  AppDB.getInstance().pokemonsDao().getById(pokemonId);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }
}
