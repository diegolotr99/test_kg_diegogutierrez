package pokedex.konradgroup.com.pokedex.modules.pokemons;

import pokedex.konradgroup.com.pokedex.models.Pokemon;
import pokedex.konradgroup.com.pokedex.modules.baseComponents.BaseView;

public interface PokemonsView extends BaseView {

        void showPokemon(final long pokemonId, final String pokemonServerId);

        void hideRefreshLoading();
}
