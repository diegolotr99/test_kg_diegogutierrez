package pokedex.konradgroup.com.pokedex.modules.main;

import pokedex.konradgroup.com.pokedex.modules.baseComponents.BaseViewModel;
import pokedex.konradgroup.com.pokedex.general.PokeWorld;
import org.greenrobot.eventbus.Subscribe;
public class MainViewModel extends BaseViewModel<MainView> {
    public MainViewModel() {
        PokeWorld.getEventBus().register(this);
    }
    @Override
    public void onStart() {
        if (!PokeWorld.getEventBus().isRegistered(this)) {
            PokeWorld.getEventBus().register(this);
        }
    }

    @Override
    public void onStop() {
        PokeWorld.getEventBus().unregister(this);

    }
    @Subscribe
    public void onSetTitleEvent(SetTitleEvent event) {
        getView().setNavigationTitle(event.getTitle());
    }
}
