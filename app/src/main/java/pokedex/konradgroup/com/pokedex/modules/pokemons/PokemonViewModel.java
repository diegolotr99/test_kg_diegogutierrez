package pokedex.konradgroup.com.pokedex.modules.pokemons;

import android.arch.lifecycle.LiveData;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import pokedex.konradgroup.com.pokedex.models.Pokemon;
import pokedex.konradgroup.com.pokedex.repository.local.AppDB;
import pokedex.konradgroup.com.pokedex.repository.remote.PokemonRepository;
import pokedex.konradgroup.com.pokedex.modules.baseComponents.BaseViewModel;

public class PokemonViewModel extends BaseViewModel<PokemonsView> {
    private PokemonRepository pokemonRepository;
    private static PokemonViewModel instance;

    public PokemonViewModel( ) {
        super();
        this.pokemonRepository = new PokemonRepository();
        instance = this;
    }

    public static PokemonViewModel getInstance() {
        return instance;
    }

    LiveData<List<Pokemon>> getPokemonList() {
        LiveData<List<Pokemon>> pokemons =  AppDB.getInstance().pokemonsDao().getAll();
        return pokemons;
    }
    public void loadPokemonList(RecyclerView recyclerView) {
        // Setup List
        PokemonListAdapter listAdapter = new PokemonListAdapter(getView());
        getPokemonList().observe(getView().getCurrentActivity(), pokemons -> {
            listAdapter.setPokemonList(pokemons);
            getView().hideRefreshLoading();
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getView().getCurrentActivity()));
        recyclerView.setAdapter(listAdapter);
        // request latest pokemons
        refreshPokemonList();


    }
    public void refreshPokemonList() {
        // request latest pokemons
        pokemonRepository.getPokemons();

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }
}


