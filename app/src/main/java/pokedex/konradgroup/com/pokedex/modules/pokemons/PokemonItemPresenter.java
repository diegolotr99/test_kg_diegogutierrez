package pokedex.konradgroup.com.pokedex.modules.pokemons;

import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import pokedex.konradgroup.com.pokedex.R;
import pokedex.konradgroup.com.pokedex.models.Pokemon;
import pokedex.konradgroup.com.pokedex.repository.remote.PokemonRepository;
import pokedex.konradgroup.com.pokedex.utils.PokemonSpritesHelper;

public class PokemonItemPresenter extends BaseObservable {
    private Pokemon pokemon;
    private PokemonsView pokemonsView;

    public PokemonItemPresenter(Pokemon pokemon, PokemonsView pokemonsView) {
        this.pokemon = pokemon;
        this.pokemonsView = pokemonsView;
    }


    public String getPokemonData() {
        return pokemon.getName();
    }
    public String getPokemonImage(){
        return PokemonSpritesHelper.FRONT_DEFAULT_URL+pokemon.getId()+".png";
    }

    @BindingAdapter({"android:src"})
    public static void setPokemonImage(ImageView imageView, String image) {
        Picasso.get()
                .load(image)
                .placeholder(R.drawable.bg_blue_tiny_borders)
                .error(R.drawable.bg_blue_tiny_borders)
                .into(imageView);
    }
    public void onItemClick(View view) {
        pokemonsView.showPokemon(pokemon.getLocalId(), pokemon.getId());
    }
    public void setPokemon(Pokemon pokemon) {
        this.pokemon = pokemon;
        notifyChange();
    }
}
