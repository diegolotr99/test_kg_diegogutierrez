package pokedex.konradgroup.com.pokedex.general;
import org.greenrobot.eventbus.EventBus;
public class PokeWorld {

    public static EventBus getEventBus() {
        return EventBus.getDefault();
    }
}
