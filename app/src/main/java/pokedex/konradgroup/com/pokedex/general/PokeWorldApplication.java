package pokedex.konradgroup.com.pokedex.general;

import android.app.Application;

import pokedex.konradgroup.com.pokedex.R;
import pokedex.konradgroup.com.pokedex.repository.local.AppDB;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class PokeWorldApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize DB INSTANCE
        AppDB.getAppDatabase(this);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/PocketMonk.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
