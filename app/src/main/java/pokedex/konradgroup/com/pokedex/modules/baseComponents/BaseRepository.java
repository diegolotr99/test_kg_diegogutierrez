package pokedex.konradgroup.com.pokedex.modules.baseComponents;

import pokedex.konradgroup.com.pokedex.general.exceptions.RestException;
import pokedex.konradgroup.com.pokedex.repository.remote.GsonStringConverterFactory;
import pokedex.konradgroup.com.pokedex.repository.remote.ResponseListener;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class BaseRepository <T extends Object>{
    protected final Retrofit retrofit;

    public BaseRepository() {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        retrofit = new Retrofit.Builder()
                .baseUrl("https://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(new GsonStringConverterFactory())
                .client(httpClientBuilder.build())
                .build();
    }

    protected void handleResponse( Response<T> response, int expectedStatusCode, ResponseListener<T> listener) {
        if (expectedStatusCode == response.code()) {
            listener.successResponserHandler(response);
        } else if (response.errorBody() != null) {
            listener.errorResponseHandler(RestException.create(response.errorBody()));
        }else {
            listener.errorResponseHandler(new Exception("Unexpected error"));
        }
    }

}
