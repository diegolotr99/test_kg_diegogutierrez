package pokedex.konradgroup.com.pokedex.modules.pokemons

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_detail_pokemon.*
import pokedex.konradgroup.com.pokedex.R
import pokedex.konradgroup.com.pokedex.modules.baseComponents.BaseFragment
import pokedex.konradgroup.com.pokedex.databinding.FragmentDetailPokemonBinding
import pokedex.konradgroup.com.pokedex.models.Pokemon
import pokedex.konradgroup.com.pokedex.repository.remote.PokemonRepository
import pokedex.konradgroup.com.pokedex.utils.PokemonSpritesHelper

class DetailPokemonFragment : BaseFragment<DetailPokemonViewModel, FragmentDetailPokemonBinding>(), DetailPokemonView, View.OnClickListener {

    lateinit var pokemon: Pokemon
    override fun getViewModel(): DetailPokemonViewModel {
        return ViewModelProviders.of(this).get(DetailPokemonViewModel::class.java)
    }

    private var pokemonId: Long = 0

    private lateinit var pokemonServerId: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pokemonId = arguments?.getLong(POKEMON_ID) ?: 0
        pokemonServerId = arguments?.getString(POKEMON_SERVER_ID).toString()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        dataBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_detail_pokemon, container, false)
        initUI()


        return dataBinding.root
    }

    private fun getPokemonDetails() {
        var pokemonRepository = PokemonRepository()
        pokemonRepository.getPokemonDetails(pokemonServerId)
        pokemon = viewModel.getPokemon(pokemonId)
    }

    private fun initUI() {
        getPokemonDetails()
        //viewModel.setTitle(pokemon.name)

        dataBinding.txtWeightTitle.visibility = ViewGroup.INVISIBLE
        dataBinding.txtWeight.visibility = ViewGroup.INVISIBLE
        dataBinding.txtBaseExperienceTitle.visibility = ViewGroup.INVISIBLE
        dataBinding.txtBaseExperience.visibility = ViewGroup.INVISIBLE

        dataBinding.textTitleName.text = pokemon.name
        dataBinding.txtWeight.text = pokemon.weight.toString()
        dataBinding.txtBaseExperience.text = pokemon.baseExperience.toString()
        dataBinding.btnMoreDetails.setOnClickListener(this)
        setImages()

    }

    override fun onClick(v: View) {
        when (v.id) {
            btnMoreDetails.id -> {
                getPokemonDetails()

                dataBinding.txtWeightTitle.visibility = ViewGroup.VISIBLE
                dataBinding.txtWeight.visibility = ViewGroup.VISIBLE
                dataBinding.txtBaseExperienceTitle.visibility = ViewGroup.VISIBLE
                dataBinding.txtBaseExperience.visibility = ViewGroup.VISIBLE

                dataBinding.txtWeight.text = pokemon.weight.toString()
                dataBinding.txtBaseExperience.text = pokemon.baseExperience.toString()

            }
        }
    }

    private fun setImages() {
        Picasso.get()
                .load(PokemonSpritesHelper.FRONT_DEFAULT_URL + pokemon.id + ".png")
                .placeholder(R.drawable.bg_blue_tiny_borders)
                .error(R.drawable.bg_blue_tiny_borders)
                .into(dataBinding.imgFront)

        Picasso.get()
                .load(PokemonSpritesHelper.BACK_DEFAULT_URL + pokemon.id + ".png")
                .placeholder(R.drawable.bg_blue_tiny_borders)
                .error(R.drawable.bg_blue_tiny_borders)
                .into(dataBinding.imgBack)

    }

    companion object {

        private val POKEMON_ID = "pokemonId"
        private val POKEMON_SERVER_ID = "pokemonServerId"
        fun newInstance(pokemonId: Long, pokemondServerId: String): DetailPokemonFragment {
            val fragment = DetailPokemonFragment()
            val args = Bundle()
            args.putLong(POKEMON_ID, pokemonId)
            args.putString(POKEMON_SERVER_ID, pokemondServerId)
            fragment.arguments = args
            return fragment
        }

        fun getInstance() {
            return
        }
    }
}